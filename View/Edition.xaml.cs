﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BLL;
using Model;


namespace View
{
    /// <summary>
    /// Interaction logic for Edition.xaml
    /// </summary>
    public partial class Edition : Window
    {
        public FenetrePrincipale FenetrePrincipale { get; set; }
        public int indexContact;

        public Edition(FenetrePrincipale Fenetre, int index)
        {
            InitializeComponent();

            this.FenetrePrincipale = Fenetre;
            this.FenetrePrincipale.Hide();
            this.Show();

            this.indexContact = index;

            this.txtNom.Text = this.FenetrePrincipale.Compte.Contacts.ElementAt(index).Nom;
            this.txtprenom.Text = this.FenetrePrincipale.Compte.Contacts.ElementAt(index).Prenom;
            this.numerotel.Text = this.FenetrePrincipale.Compte.Contacts.ElementAt(index).Telephone;

        }



        private void Message_Click(object sender, RoutedEventArgs e)
        {
            Contact contact = new Contact();

            if (!Ajouter.TxtVide(this.txtNom.Text)) {
                contact.Nom = this.txtNom.Text;
            }
            else{
                contact.Nom = this.FenetrePrincipale.Compte.Contacts.ElementAt(this.indexContact).Nom;
            }

            if (!Ajouter.TxtVide(this.txtprenom.Text))
            {
                contact.Prenom = this.txtprenom.Text;
            }
            else
            {
                contact.Prenom = this.FenetrePrincipale.Compte.Contacts.ElementAt(this.indexContact).Prenom;
            }

            if (!Ajouter.TxtVide(this.numerotel.Text))
            {
                contact.Telephone = this.numerotel.Text;
            }
            else
            {
                contact.Telephone = this.FenetrePrincipale.Compte.Contacts.ElementAt(this.indexContact).Telephone;
            }

            this.FenetrePrincipale.Gerant.EditionContact(contact, this.indexContact);
            this.FenetrePrincipale.ListerContacts();
            this.FenetrePrincipale.Show();
            this.Close();
        }

        private void Txtpassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FenetrePrincipale.Show();
        }

        private void InputNumero(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using BLL;




namespace View
{
    /// <summary>
    /// Interaction logic for FenetrePrincipale.xaml
    /// </summary>
    public partial class FenetrePrincipale : Window
    {
        public MainWindow fenetreLogin;
        public Rechercher Rechercher { get; set; } 
        public Ajouter Ajouter { get; set; }
        public Edition EditionFenetre { get; set; }
        public Trier Trier { get; set; }

        public Gerant Gerant { get; set; }
        public Compte Compte { get; set; }

        //-------------------------------------------------------------------------
        public FenetrePrincipale(MainWindow fenetreLogin)
        {
            InitializeComponent();
            this.Height = 300;

            this.fenetreLogin = fenetreLogin;

            if (UserExists(this.fenetreLogin.txtusername.Text, this.fenetreLogin.txtpassword.Password))
            {
                this.Gerant = new Gerant(this.fenetreLogin.txtusername.Text);

                this.Compte = this.Gerant.Compte;

                this.fenetreLogin.not_found.Visibility = Visibility.Hidden;

                this.ListeContact.Visibility = Visibility.Hidden;

                this.Show();
                this.fenetreLogin.Hide();
            }
            else
            {
                this.Close();

                this.fenetreLogin.not_found.Visibility = Visibility.Visible;
            }

        }

        public bool UserExists(string username, string password)
        {
            return Gerant.UserExistant(username, password);
        }

        public void ListerContacts()
        {
            this.ListeContact.Items.Clear();

            for (int i = 0; i < this.Compte.Contacts.Count; i++)
            {
                Contact contact = this.Compte.Contacts.ElementAt(i);
                this.ListeContact.Items.Add(contact);
            }
        }

        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.fenetreLogin.Show();
        }

        private void Affichage_Click(object sender, RoutedEventArgs e)
        {            
            if (this.ListeContact.Visibility == Visibility.Hidden)
            {
                this.Gerant.LoadContacts();

                this.ListerContacts();

                this.Height = 686;
                this.ListeContact.Visibility = Visibility.Visible;
                this.Affichage.Content = "1-Cacher les contacts";
            }
            else if (this.ListeContact.Visibility == Visibility.Visible)
            {
                this.Height = 300;
                this.ListeContact.Visibility = Visibility.Hidden;
                this.Affichage.Content = "1-Affichage de tous les contacts";
            }
        }


        private void Ajout_Click(object sender, RoutedEventArgs e)
        {
            this.Ajouter = new Ajouter(this);
        }

        private void Suppression_Click(object sender, RoutedEventArgs e)
        {
            if(this.ListeContact.SelectedIndex != -1)
            {
                this.Gerant.SupprimerContact(this.ListeContact.SelectedIndex);
                this.ListerContacts();
            }
        }

        private void Edition_Click(object sender, RoutedEventArgs e)
        {
            if (this.ListeContact.SelectedIndex != -1)
            {
                this.EditionFenetre = new Edition(this, this.ListeContact.SelectedIndex);
            }
        }

        private void Recherche_Click(object sender, RoutedEventArgs e)
        {
            this.Rechercher = new Rechercher(this);
        }

        private void Tri_Click(object sender, RoutedEventArgs e)
        {
           this.Trier = new Trier(this);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}

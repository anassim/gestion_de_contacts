﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public FenetrePrincipale FenetrePrincipale { get; set; }
        public AjouterCompte FenetreAjouterCompte { get; set; }

        //-----------------------------------------------------------------------

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            this.FenetrePrincipale = new FenetrePrincipale(this);
        }

        private void Creer_Compte_Click(object sender, MouseButtonEventArgs e)
        {
            this.FenetreAjouterCompte = new AjouterCompte(this);
        }

        private void Souligner_Create_Account(object sender, MouseEventArgs e)
        {
            this.CreerCompte.TextDecorations = TextDecorations.Underline;
        }

        private void Banaliser_Create_Account(object sender, MouseEventArgs e)
        {
            this.CreerCompte.TextDecorations = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BLL;
using Model;


namespace View
{
    /// <summary>
    /// Interaction logic for AjouterCompte.xaml
    /// </summary>
    public partial class AjouterCompte : Window
    {
        public MainWindow FenetrePrincipale { get; set; }
        public AjouterCompte(MainWindow fenetrePrincipale)
        {
            InitializeComponent();

            this.FenetrePrincipale = fenetrePrincipale;
            this.FenetrePrincipale.Hide();
            this.Show();
        }

        private void Ajouter_Click(object sender, RoutedEventArgs e)
        {
            if (!Ajouter.TxtVide(this.txtusername.Text) && !Ajouter.TxtVide(this.txtpassword.Password) && !Ajouter.TxtVide(this.ConfirmePassword.Password))
            {
                if (!File.Exists(Environment.CurrentDirectory + @"/Comptes/" + this.txtusername.Text + ".txt"))
                {
                    if (this.txtpassword.Password == this.ConfirmePassword.Password)
                    {
                        if (!File.Exists(@"/Comptes/" + this.txtusername.Text + ".txt"))
                        {
                            Gerant.AjouterCompte(this.txtusername.Text, this.txtpassword.Password);
                            this.Close();
                        }
                        else
                        {
                            this.userInfo.Content = "utilisateur existant";
                        }
                    }
                    else
                    {
                        this.userInfo.Content = "les passwords ne correspondent pas";
                    }
                }
                else
                {
                    this.userInfo.Content = "le user existe deja";
                }
            }
            else
            {
                this.userInfo.Content = "champs non remplies";
            }

            this.userInfo.Visibility = Visibility.Visible;

        }

        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FenetrePrincipale.Show();
        }
    }
}

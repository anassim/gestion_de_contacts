﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BLL;
using Model;


namespace View
{
    /// <summary>
    /// Interaction logic for Trier.xaml
    /// </summary>
    public partial class Trier : Window
    {
        public FenetrePrincipale FenetrePrincipale { get; set; }
        public Trier(FenetrePrincipale Fenetre)
        {
            InitializeComponent();

            this.FenetrePrincipale = Fenetre;
            this.FenetrePrincipale.Hide();
            this.Show();
        }

        private void Message_Click1(object sender, RoutedEventArgs e)
        {
            this.FenetrePrincipale.Compte.Contacts.Sort(delegate (Contact x, Contact y) { return x.Nom.CompareTo(y.Nom); });
            this.FenetrePrincipale.ListerContacts();

            this.FenetrePrincipale.Height = 686;
            this.FenetrePrincipale.ListeContact.Visibility = Visibility.Visible;
            this.FenetrePrincipale.Affichage.Content = "1-Cacher les contacts";

            this.Close();
        }

        private void Message_Click2(object sender, RoutedEventArgs e)
        {
            this.FenetrePrincipale.Compte.Contacts.Sort(delegate (Contact x, Contact y) { return x.Prenom.CompareTo(y.Prenom); });
            this.FenetrePrincipale.ListerContacts();

            this.FenetrePrincipale.Height = 686;
            this.FenetrePrincipale.ListeContact.Visibility = Visibility.Visible;
            this.FenetrePrincipale.Affichage.Content = "1-Cacher les contacts";

            this.Close();
        }

        private void Message_Click3(object sender, RoutedEventArgs e)
        {
            this.FenetrePrincipale.Compte.Contacts.Sort(delegate (Contact x, Contact y) { return x.Telephone.CompareTo(y.Telephone); });
            this.FenetrePrincipale.ListerContacts();

            this.FenetrePrincipale.Height = 686;
            this.FenetrePrincipale.ListeContact.Visibility = Visibility.Visible;
            this.FenetrePrincipale.Affichage.Content = "1-Cacher les contacts";

            this.Close();
        }


        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FenetrePrincipale.Show();
        }
    }
}

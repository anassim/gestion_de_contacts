﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BLL;
using Model;


namespace View
{
    /// <summary>
    /// Interaction logic for Rechercher.xaml
    /// </summary>
    public partial class Rechercher : Window
    {
        public FenetrePrincipale FenetrePrincipale { get; set; }
        public Rechercher(FenetrePrincipale Fenetre)
        {
            InitializeComponent();

            this.FenetrePrincipale = Fenetre;
            this.FenetrePrincipale.Hide();
            this.Show();
        }

        private void Txtpassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Txtus_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Message_Click(object sender, RoutedEventArgs e)
        {
            List<Contact> cont = new List<Contact>();

            foreach(Contact c in this.FenetrePrincipale.Compte.Contacts)
            {
                    if (this.txtnom.Text.Length <= c.Nom.Length && !Ajouter.TxtVide(this.txtnom.Text) && c.Nom.Substring(0, this.txtnom.Text.Length) == this.txtnom.Text)
                    {
                        cont.Add(c);
                    }


                    else if (this.txtprenom.Text.Length <= c.Prenom.Length && !Ajouter.TxtVide(this.txtprenom.Text) && c.Prenom.Substring(0, this.txtprenom.Text.Length) == this.txtprenom.Text)
                    {
                        cont.Add(c);
                    }

                    else if (this.numerotel.Text.Length <= c.Telephone.Length && !Ajouter.TxtVide(this.numerotel.Text) && c.Telephone.Substring(0, this.numerotel.Text.Length) == this.numerotel.Text)
                    {
                        cont.Add(c);
                    }
            }

            this.FenetrePrincipale.Compte.Contacts.Clear();
            this.FenetrePrincipale.Compte.Contacts = cont;

            this.FenetrePrincipale.ListerContacts();
            this.FenetrePrincipale.Gerant.LoadContacts();

            this.FenetrePrincipale.Height = 686;
            this.FenetrePrincipale.ListeContact.Visibility = Visibility.Visible;
            this.FenetrePrincipale.Affichage.Content = "1-Cacher les contacts";

            this.Close();
        }

        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FenetrePrincipale.Show();
        }

        private void InputNumero(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}

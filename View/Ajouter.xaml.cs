﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BLL;
using Model;


namespace View
{
    /// <summary>
    /// Interaction logic for Ajouter.xaml
    /// </summary>
    public partial class Ajouter : Window
    {
        public FenetrePrincipale FenetrePrincipale { get; set; }
        public Ajouter(FenetrePrincipale Fenetre)
        {
            InitializeComponent();

            this.FenetrePrincipale = Fenetre;
            this.FenetrePrincipale.Hide();
            this.Show();
        }

        private void Txtus_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Txtpassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Numerotel_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Message_Click(object sender, RoutedEventArgs e)
        {
            if (!Ajouter.TxtVide(this.txtNom.Text) && !Ajouter.TxtVide(this.txtpassword.Text) && !Ajouter.TxtVide(this.numerotel.Text))
            {
                this.FenetrePrincipale.Gerant.AjouterContact(new Contact(this.txtNom.Text, this.txtpassword.Text, this.numerotel.Text));
                this.FenetrePrincipale.ListerContacts();
                this.FenetrePrincipale.Show();
                this.Close();
            }    
        }
        public static bool TxtVide(string txt)
        {
            bool isEmpty = true;

            if(!string.IsNullOrEmpty(txt))
            {
               foreach(char lettre in txt)
                {
                    if (lettre != ' ')
                    {
                        isEmpty = false;
                    }
                }
            }


            return isEmpty;
        }

        private void Fermeture(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FenetrePrincipale.Show();
        }

        private void InputNumero(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}

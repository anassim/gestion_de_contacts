﻿using System;
using System.Collections.Generic;
using System.IO;
using Model;

namespace DAL
{
    public class Admin
    {
        public Compte Compte { get; set; }

        //---------------------------------------------------------------

        public Admin(string userName)
        {
            this.Compte = new Compte(userName);
            this.LoadContacts();
        }

        public void LoadContacts()
        {

            this.Compte.Contacts.Clear();

            string nomFichier = Environment.CurrentDirectory + @"\Comptes\" + this.Compte.UserName + ".txt";
            string ligneFichier;
            string[] listeInfoUser;

            if (File.ReadAllText(nomFichier) != null)
            {
                StreamReader fichierContact = new StreamReader(nomFichier);
                do
                {
                    ligneFichier = fichierContact.ReadLine();
                    if (ligneFichier != null)
                    {
                        listeInfoUser = ligneFichier.Split('%');
                        this.Compte.Contacts.Add(new Contact(listeInfoUser[0], listeInfoUser[1], listeInfoUser[2]));
                    }
                } while (ligneFichier != null);
                fichierContact.Close();
            }
        }

        public static bool UserExistant(string user, string password)
        {
            bool ilExist = false;
            if (File.Exists(Environment.CurrentDirectory + @"\Comptes\" + user + ".txt"))
            {
                foreach(string ligne in File.ReadAllLines(Environment.CurrentDirectory + @"\Comptes\Passwords.txt"))
                {
                    if (ligne.Split('~')[0] == user)
                    {
                        if (ligne.Split('~')[1] == password)
                        {
                            ilExist = true;
                        }
                    }
                }

            }
            return ilExist;
        }

        public void AjouterContactFichier(Contact contact)
        {
            string nomFichier = Environment.CurrentDirectory + @"\Comptes\" + this.Compte.UserName + ".txt";

            StreamWriter fichierContact = File.AppendText(nomFichier);

            fichierContact.WriteLine(contact.Nom + "%" + contact.Prenom + "%" + contact.Telephone);

            fichierContact.Close();
        }

        public void SupprimerContactFichier(int index)
        {
            string nomFichier = Environment.CurrentDirectory + @"\Comptes\" + this.Compte.UserName + ".txt";

            List<string> listeDesContacts = new List<string>();
            listeDesContacts.AddRange(File.ReadAllLines(nomFichier));
            listeDesContacts.RemoveAt(index);

            File.WriteAllLines(nomFichier, listeDesContacts);
        }

        public void EditionContactFichier(Contact contact, int index)
        {
            string nomFichier = Environment.CurrentDirectory + @"\Comptes\" + this.Compte.UserName + ".txt";
            List<string> listeDesContacts = new List<string>();
            listeDesContacts.AddRange(File.ReadAllLines(nomFichier));
            listeDesContacts.RemoveAt(index);
            listeDesContacts.Insert(index, contact.Nom + "%" + contact.Prenom + "%" + contact.Telephone);

            File.WriteAllLines(nomFichier, listeDesContacts);
        }


        public static void AjouterCompteFichier(string username, string password)
        {
            File.Create(Environment.CurrentDirectory + @"/Comptes/" + username + ".txt").Close();

            string compte = username + "~" + password;
            File.AppendAllText(Environment.CurrentDirectory + @"/Comptes/Passwords.txt", "\n" + compte);
        }

    }
}

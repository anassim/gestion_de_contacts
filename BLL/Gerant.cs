﻿using System;
using Model;
using DAL;

namespace BLL
{
    public class Gerant
    {
        public Admin Admin { get; set; }

        public Compte Compte { get; set; }

        //---------------------------------------------------------------

        public Gerant(string userName)
        {
            this.Admin = new Admin(userName);

            this.Compte = this.Admin.Compte;
        }

        public static bool UserExistant(string username, string password)
        {
           return Admin.UserExistant(username, password);
        }

        public void LoadContacts()
        {
            this.Admin.LoadContacts();
        }

        public void AjouterContact(Contact contact)
        {
            this.Admin.AjouterContactFichier(contact);
            this.Admin.LoadContacts();
        }

        public void SupprimerContact(int index)
        {
            this.Admin.SupprimerContactFichier(index);
            this.Admin.LoadContacts();
        }

        public void EditionContact(Contact contact, int indexContact)
        {
            this.Admin.EditionContactFichier(contact, indexContact);
            this.Admin.LoadContacts();

        }

        public static void AjouterCompte(string username, string password)
        {
            Admin.AjouterCompteFichier(username, password);
        }
    }
}

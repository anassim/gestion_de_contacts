﻿using System;


namespace Model
{
    public class Contact
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Telephone { get; set; }


        public Contact()
        {

        }

        public Contact(string nom, string prenom, string tel = null)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Telephone = tel;
        }

    }
}

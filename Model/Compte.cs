﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class Compte
    {
        public List<Contact> Contacts { get; set; }
        public string UserName { get; set; }


        public Compte()
        {
            this.Contacts = new List<Contact>();
            this.UserName = null;
        }
        public Compte(string username)
        {
            this.Contacts = new List<Contact>();
            this.UserName = username;
        }
    }
}
